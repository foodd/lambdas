output "namespace" {
  value = "${var.namespace}"
}

output "stage" {
  value = "${var.stage}"
}

output "environment" {
  value = "${var.environment}"
}

output "name" {
  value = "${var.name}"
}

output "delimiter" {
  value = "${var.delimiter}"
}

output "attributes" {
  value = "${var.attributes}"
}

output "tags" {
  value = "${var.tags}"
}

output "region" {
  value = "${var.region}"
}

# CALLER IDENTITY

output "account_id" {
  value = "${data.aws_caller_identity.this.account_id}"
}

output "user_id" {
  value = "${data.aws_caller_identity.this.user_id}"
}

output "arn" {
  value = "${data.aws_caller_identity.this.arn}"
}

# LABEL

output "label" {
  value = {
    id         = "${module.label.id}"
    name       = "${module.label.name}"
    namespace  = "${module.label.namespace}"
    stage      = "${module.label.stage}"
    attributes = "${module.label.attributes}"
  }
}

output "label_tags" {
  value = "${module.label.tags}"
}

# MODULE SSH KEY

output "key_pair_name" {
  description = "Name of SSH key"
  value       = "${module.key_pair.key_name}"
}

output "key_pair_private_key_filename" {
  description = "Private Key Filename"
  value       = "${module.key_pair.private_key_filename}"
}

output "key_pair_public_key" {
  description = "Contents of the generated public key"
  value       = "${module.key_pair.public_key}"
}

output "key_pair_public_key_filename" {
  description = "Public Key Filename"
  value       = "${module.key_pair.public_key_filename}"
}

# VPC

output "azs" {
  description = "A list of availability zones specified as argument to this module"

  value = "${module.vpc.azs}"
}

output "public_network_acl_id" {
  description = "ID of the public network ACL"

  value = "${module.vpc.public_network_acl_id}"
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"

  value = "${module.vpc.public_route_table_ids}"
}

output "public_subnets" {
  description = "List of IDs of public subnets"

  value = "${module.vpc.public_subnets}"
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"

  value = "${module.vpc.public_subnets_cidr_blocks}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"

  value = "${module.vpc.vpc_cidr_block}"
}

output "vpc_enable_dns_hostnames" {
  description = "Whether or not the VPC has DNS hostname support"

  value = "${module.vpc.vpc_enable_dns_hostnames}"
}

output "vpc_enable_dns_support" {
  description = "Whether or not the VPC has DNS support"

  value = "${module.vpc.vpc_enable_dns_support}"
}

output "vpc_id" {
  description = "The ID of the VPC"

  value = "${module.vpc.vpc_id}"
}

output "vpc_instance_tenancy" {
  description = "Tenancy of instances spin up within VPC"

  value = "${module.vpc.vpc_instance_tenancy}"
}

output "vpc_main_route_table_id" {
  description = "The ID of the main route table associated with this VPC"

  value = "${module.vpc.vpc_main_route_table_id}"
}

output "vpc_secondary_cidr_blocks" {
  description = "List of secondary CIDR blocks of the VPC"

  value = "${module.vpc.vpc_secondary_cidr_blocks}"
}
