import boto3


def lambda_handler(event, context):
    resource = boto3.resource('ec2')
    # Get only EC2 instances that are: running AND are tagged as DailyStartup
    filters = [{'Name': 'instance-state-name', 'Values': ['stopped']},
               {'Name': 'tag:DailyStartup', 'Values': ['true']}]

    instances = resource.instances.filter(Filters=filters)
    if len(list(instances)) > 0:
        print("Starting up instances")
        for instance in instances:
            print("Instance ID: ", instance.id)
            instance.start()
    else:
        print("No instances to be started at this moment")
