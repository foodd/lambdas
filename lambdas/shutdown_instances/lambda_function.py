import boto3


def lambda_handler(event, context):
    resource = boto3.resource('ec2')
    # Get only EC2 instances that are: running AND are tagged as DailyShutdown
    filters = [{'Name': 'instance-state-name', 'Values': ['running']},
               {'Name': 'tag:DailyShutdown', 'Values': ['true']}]

    instances = resource.instances.filter(Filters=filters)
    if len(list(instances)) > 0:
        print("Shutting down instances")
        for instance in instances:
            print("Instance ID: ", instance.id)
            instance.stop()
    else:
        print("No instances to be stopped at this moment")
